package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" this is called as "Annotations" mark.
	// Annotations are used to provide supplemental information about the program.
	// These are used to manage and configure the behavior of the framework.
	// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.
@SpringBootApplication
// Tells the Spring boot that this will handle endpoints for web request
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	// This is used for mapping HTTP GET requests.
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters.
		// name = john; Hello john.
		// name = World; Hello World.
		// To appned the URL with a name parameter we do the ff:
			// http:localhost:8080/hello?name=john
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user!") String user){
		return String.format("Hi %s", user);
	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "name") String name, @RequestParam(value = "age", defaultValue = "0") Integer age){
		return String.format("Hi %s! Your age is %d", name, age);
	}
}
